<?php

declare(strict_types=1);

namespace Gnom\Config\Api;

interface ReaderInterface
{
    /**
     * @return array
     */
    public function read(): array;
}