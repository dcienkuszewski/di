<?php

declare(strict_types=1);

namespace Gnom\Config\Api;

interface SourceInterface
{
    /**
     * @return string
     */
    public function getFilename(): string;
}