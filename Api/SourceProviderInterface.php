<?php

declare(strict_types=1);

namespace Gnom\Config\Api;

interface SourceProviderInterface
{
    /**
     * @return SourceInterface[]
     */
    public function provide(): array;
}