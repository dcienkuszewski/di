<?php

declare(strict_types=1);

namespace Gnom\Config\Model\Writer;

use Symfony\Component\Yaml\Yaml;

use function file_put_contents;

class File
{
    /**
     * @param string $file
     * @param array $data
     */
    public function write(string $file, array $data): void
    {
        $yaml = Yaml::dump($data);
        file_put_contents($file, $yaml);
    }
}