<?php

declare(strict_types=1);

namespace Gnom\Config\Model;

use Gnom\Config\Api\ReaderInterface;
use Gnom\Config\Model\Reader\File;
use Gnom\Config\Model\Reader\File\SourcePool;

class ConfigReader implements ReaderInterface
{
    private SourcePool $sourcePool;

    private File $file;

    public function __construct(SourcePool $sourcePool, File $file)
    {
        $this->sourcePool = $sourcePool;
        $this->file = $file;
    }

    public function read(): array
    {
        $files = [];
        foreach ($this->sourcePool->getSources() as $source) {
            $files[] = $source->getFilename();
        }
        return $this->file->read($files);
    }
}