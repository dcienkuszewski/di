<?php

declare(strict_types=1);

namespace Gnom\Config\Model\Reader;

use Gnom\Application\ObjectManagerInterface;
use Gnom\Config\Model\ConfigMerger;
use Symfony\Component\Yaml\Yaml;

class File
{
    private ObjectManagerInterface $objectManager;

    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param string[] $files
     *
     * @return string[]
     */
    public function read(array $files): array
    {
        $configMerger = $this->objectManager->create(ConfigMerger::class);
        foreach ($files as $file) {
            if (file_exists($file)) {
                $configMerger->merge(Yaml::parseFile($file));
            }
        }

        return $configMerger->getMergedConfig();
    }
}