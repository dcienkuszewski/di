<?php

declare(strict_types=1);

namespace Gnom\Config\Model\Reader\File\Source;

use Gnom\Application\DirectoryList;
use Gnom\Application\ObjectManagerInterface;
use Gnom\Config\Api\ConfigFileInterface;
use Gnom\Config\Api\SourceInterface;
use Gnom\Config\Api\SourceProviderInterface;

class GlobalSourceProvider implements SourceProviderInterface
{
    private DirectoryList $directoryList;

    private ObjectManagerInterface $objectManager;

    public function __construct(
        DirectoryList $directoryList,
        ObjectManagerInterface $objectManager
    ) {
        $this->directoryList = $directoryList;
        $this->objectManager = $objectManager;
    }

    /**
     * @inheritDoc
     */
    public function provide(): array
    {
        $filename = $this->directoryList->getPath(DirectoryList::CODE_APP, ConfigFileInterface::FILE_PATH_PATTERN);
        return [
            $this->objectManager->create(SourceInterface::class, ['filename' => $filename])
        ];
    }

}