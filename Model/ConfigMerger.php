<?php

declare(strict_types=1);

namespace Gnom\Config\Model;

class ConfigMerger
{
    private array $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function merge(array $data): void
    {
        $this->data = array_merge_recursive($this->data, $data);
    }

    public function getMergedConfig(): array
    {
        return $this->data;
    }
}